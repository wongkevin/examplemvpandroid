package com.example.applicationmvp;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MiAdaptador extends BaseAdapter {
    private Context context;
    private int layout;
    private List<String> nombres;

    public MiAdaptador(Context contexto, int layout, List<String> nombres){
        this.context = contexto;
        this.layout =layout;
        this.nombres = nombres;
    }

    @Override
    public int getCount() {
        return this.nombres.size();
    }

    @Override
    public Object getItem(int position) {
        return this.nombres.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        //View vista que llega
        View vista = convertView;
        //Inflamos la vista de llegada con nuestro layout persnalizada
        LayoutInflater layoutInflater = LayoutInflater.from(this.context);
        vista = layoutInflater.inflate(R.layout.lista_items,null);

        //Tener el valor actual dependiente de la posicion
        String nombreActual = nombres.get(position);
        //Referenciamos el evento a modificar y lo rellenamos
        TextView textView = (TextView) vista.findViewById(R.id.textView);
        textView.setText(nombreActual);
        return  vista;

        /*
        ViewHolder viewHolder;

        if(convertView == null){
            convertView = LayoutInflater.from(context).inflate(R.layout.lista_items, null);
            viewHolder = new ViewHolder(convertView);
            convertView.setTag(viewHolder);
        }else{
            viewHolder = (ViewHolder)convertView.getTag();
        }
        //rest remains the same
        //Tener el valor actual dependiente de la posicion
        String nombreActual = nombres.get(position);
        //Referenciamos el evento a modificar y lo rellenamos
        TextView textView = (TextView) convertView.findViewById(R.id.textView);
        textView.setText(nombreActual);
        return convertView;*/
    }
}

/*class ViewHolder{
        ViewHolder(View view){
            ButterKnife.bind(this,view);
        }
        @BindView(R.id.textView) TextView textoNombre;
        //@BindView(R.id.im)
    }
*/