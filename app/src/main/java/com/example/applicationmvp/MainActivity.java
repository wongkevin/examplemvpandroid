package com.example.applicationmvp;

import android.app.Activity;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import net.grandcentrix.thirtyinch.TiActivity;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MainActivity extends TiActivity<KevinPresentador,KevinView>
                            implements KevinView{
    //private TextView mSalida;
    /*private Button btnSalida;
    */
    private ListView listaAlgo;
    @BindView( R.id.textView1) TextView mSalida;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        listaAlgo = (ListView) findViewById(R.id.mobile_list);
        cargarNombresLista(100);
        //getPresenter().cargarNombres(100);
        /*List<String> nombres = new ArrayList<String>();
        nombres.add("Kevin");
        nombres.add("Jose");
        nombres.add("Kevil");
        nombres.add("Joselyn");
        nombres.add("Clavel");
        listaAlgo.setAdapter(new MiAdaptador(MainActivity.this,R.layout.lista_items,nombres));*/

        //mSalida = (TextView) findViewById(R.id.textView1);
        //btnSalida = (Button) findViewById(R.id.btnMensaje);
        //Asignar al boton el metodo click
        /*btnSalida.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                logear();
            }
        });*/
    }
    @OnClick(R.id.btnMensaje) public void onClickBtnMensaje(){
       logear();
    }
    //@NonNull
    @Override
    public KevinPresentador providePresenter() {
        //return null;
        return new KevinPresentador();
    }
    @Override
    public void mostrarTexto(String texto) {
        mSalida.setText(texto);
    }
    @Override
    public void mostrarLogearBien(String mensaje) {
        //Toast.makeText(this,"User Login",Toast.LENGTH_LONG).show();
        Toast.makeText(this,mensaje,Toast.LENGTH_LONG).show();
    }
    public void cargarNombresLista(int id) {
        //getPresenter().cargarNombres(id);
        listaAlgo.setAdapter(new MiAdaptador(MainActivity.this,R.layout.lista_items,getPresenter().cargarNombres(id)));
        listaAlgo.setOn
    }
    private void logear(){
        getPresenter().logear("Joselyn","123456");
    }
}

