package com.example.applicationmvp;

public class Usuario {
    public int idusuario;
    public String usuario;
    public String clave;
    public String correo;
    public String telefono;
    public String nombres;
    public String apellidos;
    public int idrol;

    public int getIdusuario() {
        return idusuario;
    }

    public void setIdusuario(int idusuario) {
        this.idusuario = idusuario;
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public String getClave() {
        return clave;
    }

    public void setClave(String clave) {
        this.clave = clave;
    }

    public String getCorreo() {
        return correo;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public String getNombres() {
        return nombres;
    }

    public void setNombres(String nombres) {
        this.nombres = nombres;
    }

    public String getApellidos() {
        return apellidos;
    }

    public void setApellidos(String apellidos) {
        this.apellidos = apellidos;
    }

    public int getIdrol() {
        return idrol;
    }

    public void setIdrol(int idrol) {
        this.idrol = idrol;
    }
    public Usuario(int idusuario, String usuario, String clave, String correo, String telefono, String nombres, String apellidos, int idrol) {
        this.idusuario = idusuario;
        this.usuario = usuario;
        this.clave = clave;
        this.correo = correo;
        this.telefono = telefono;
        this.nombres = nombres;
        this.apellidos = apellidos;
        this.idrol = idrol;
    }
}
