package com.example.applicationmvp;

import net.grandcentrix.thirtyinch.TiView;
import net.grandcentrix.thirtyinch.callonmainthread.CallOnMainThread;

import java.util.List;

public interface KevinView extends TiView {
    @CallOnMainThread
    void mostrarTexto(final String texto);

    @CallOnMainThread
    void mostrarLogearBien(final String mensaje);

    //@CallOnMainThread
    //void mostrarDatosNombres( List<String> nombres);
}
