package com.example.applicationmvp;

import android.support.annotation.NonNull;
import android.widget.Toast;

import net.grandcentrix.thirtyinch.TiPresenter;
import net.grandcentrix.thirtyinch.rx.RxTiPresenterSubscriptionHandler;

import java.util.ArrayList;
import java.util.List;

public class KevinPresentador extends TiPresenter<KevinView> {
    //private RxTiPresenterSubscriptionHandler rxAyuda = new RxTiPresenterSubscriptionHandler(this);


    @Override
    protected void onAttachView( KevinView view) {
        super.onAttachView(view);
        //view.mostrarTexto("Funcionando con MVP ThirtyLch");
    }
    public void logear(@NonNull String user,@NonNull String clave){
        if(user.equals("Joselyn") && clave.equals("123456"))
            getView().mostrarLogearBien("Las credenciales son correctas");
    }

    public List<String> cargarNombres(int idNombres){
        //Cargar de la base de datos los nombres
        List<String> nombres = new ArrayList<String>();
        nombres.add("Kevin");
        nombres.add("Jose");
        nombres.add("Kevil");
        nombres.add("Joselyn");
        nombres.add("Clavel");
        return  nombres;
        //getView().mostrarDatosNombres(nombres);
    }
}
